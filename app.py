from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
    "https://sadanduseless.b-cdn.net/wp-content/uploads/2014/05/short-neck16.jpg",
    "https://i.ytimg.com/vi/GEzJCSEQ13U/maxresdefault.jpg",
    "https://i.imgur.com/ZJFOLxX.jpg",
    "https://www.sunnyskyz.com/uploads/2014/03/543or-alpaca.jpg",
    "https://i.imgur.com/266NlhS.jpg",
    "https://sadanduseless.b-cdn.net/wp-content/uploads/2014/05/short-neck4.jpg",
    "https://www.sunnyskyz.com/uploads/2014/03/bhn4r-donkey.jpg",
    "https://memeguy.com/photos/images/pic-5-animals-without-necks-203658.jpg",
    "http://www.pleated-jeans.com/wp-content/uploads/2013/06/Screen-shot-2013-06-10-at-9.38.01-AM.jpg",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQn0n9UIIdilcBwUKMvp8lo3d5XRptFQ5HLk3-SVCsyicOSkBq_",
    "https://www.sunnyskyz.com/uploads/2014/03/te9cu-meerkat.jpg",
    "http://i.imgur.com/2AIkoJl.jpg",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTd3fq8-72YHF7dWnmN5lMamOb9dIJwEzyvIRyQjVb7izEBs6ioVQ",
    "http://i.imgur.com/1wDS1FI.jpg",
    "http://www.funcage.com/blog/wp-content/uploads/2013/06/Animals-Without-Necks-011.jpg",
    "https://i.ytimg.com/vi/9yqoNmDxW1g/hqdefault.jpg",
    "https://i.imgur.com/LBKpW1T.jpg",
    "https://i.pinimg.com/236x/f9/3c/cd/f93ccdb583ef5f1b1c253f17766a0565--funny-animals-wild-animals.jpg",
]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
